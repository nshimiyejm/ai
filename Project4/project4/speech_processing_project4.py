# ==============================================================================
# Project 4 Goal:  Natural Language Processing
# 				   Using speeches from US presidents,
# 				   1. Determine the majopr themes that have existed in the US over time
# 				   2. Determine presidents that have shared similar terminologies
#                  - Clustering
#                  - NLP
#                  'pip install tabulate' - for table format printing
# ==============================================================================
#   Author: Jean-Marie Nshimiyimana
#   Last updated:  4/11/2019
# ===============================================================================
# Resource used:
#            - https://www.quora.com/How-do-I-combine-multiple-text-files-into-one-text-file-using-python
#            - https://www.youtube.com/watch?v=xvqsFTUsOmc
#            - http://brandonrose.org/clustering
#            - https://www.scikit-yb.org/en/latest/api/text/freqdist.html

import os, os.path
import nltk
import pandas as pd
import numpy as np
import sklearn
import matplotlib.pyplot as plt

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.cluster import KMeans, AgglomerativeClustering
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from wordcloud import WordCloud
from lab7_dendrogram import plot_dendrogram


url = r'D:\Repository\AI_BitBucket\ai\Project4\speeches'
file_count = len([name for name in os.listdir(url) if os.path.isfile(os.path.join(url, name))])
df = pd.DataFrame()
president_speeches = []

#  Returns one single text from all the speeches
def create_corpus():
    for i in range(0, file_count):
        file_dir = os.path.join(url, os.listdir(url)[i])
        file_name = os.listdir(url)[i]
        year = file_name[:4]
        president = file_name[5:].strip('.txt')
        with open(file_dir) as f:
            text = open(file_dir).read().strip('\n')
            president_speeches.append([file_name, text])
    # return president_speeches

    # df contains all the filename, president speeches
    df = pd.DataFrame(president_speeches, columns=['filename', 'speeches'])
    return df

# Get the corpus from the 58 speeches
# The coorpus is stored as a dataframe
# The Structure is formated as:
# Key = "2017-<president_name>.txt"
# Value = "<Actual speech>"
df = create_corpus()

# Combine the text then use the combined text
# To generate a frequency count of all the words
# From teh speeches of the presidents
combined_text = '\n '.join(df['speeches'])


# In the processing phase, the algorithm:
# - removes stop words,
# - uses lemmatization to retain actual words
def process_files(text):
    # Remove punctuation
    tokenizer = RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(text)
    words = [t.lower() for t in tokens]

    lemmatizer = nltk.WordNetLemmatizer()
    lemmatized_tokens = [lemmatizer.lemmatize(w) for w in words]

    # Remove stop words
    refined_tokens = lemmatized_tokens[:]
    # sr = stopwords.words('english')
    for token in lemmatized_tokens:
        if token in stopwords.words('english'):
            refined_tokens.remove(token)
    return refined_tokens

# Process text and store it in clean data
cleaned_data = process_files(combined_text)

# Frequency analysis with lower case words with no puctuation
def frequency_analysis_word_count(freq):
    print('Frequency Analysis .....')
    print(75*'-'+'\n')
    print('{}       {}'.format('Word','Count'))
    print('{}     {}'.format('------','------'))
    for key, val in freq.items():
        print('{} : {}'.format(str(key), str(val)))
    print("Length of Unique Items: {}".format(len(freq.items())))
    freq.plot(20, cumulative=False)


# Displays a word cloud of words from each speech
def create_word_cloud(cleaned_data):
	global wordcloud
	last_refined_text = ' '.join(cleaned_data)
	wordcloud = WordCloud().generate(last_refined_text)
	plt.imshow(wordcloud, interpolation='bilinear')
	plt.axis("off")
	plt.show()

freq = nltk.FreqDist(cleaned_data)
frequency_analysis_word_count(freq)

# Create a word cloud of first uncleaned data
create_word_cloud(cleaned_data)

# If a word is less than or equals to two characters
# If a word contains number remove it from the list
# Run another frequency analysis on the cleaned version
for i in cleaned_data:
    if len(i) <=  2 or not i.isalpha():
        cleaned_data.remove(i)

freq = nltk.FreqDist(cleaned_data)
frequency_analysis_word_count(freq)

# Create a second word cloud of first cleaned data
create_word_cloud(cleaned_data)

def construct_a_dtm(data):
    '''
    - Constructing a Document term matrix that will be used to visualize the words
    - The DTM will be stored as an excel file containing all the words from each speech
    - return a document term matrix that will be used to compare each president
    '''
    cv = CountVectorizer(analyzer=process_files)
    data_cv = cv.fit_transform(data['speeches'])
    data_dtm = pd.DataFrame(data_cv.toarray(), columns=cv.get_feature_names())
    data_dtm.index = df.filename
    return data_dtm

data_dtm =  construct_a_dtm(df)

# Remove columns that have numeric headers
headers_with_numbers = []
for i in range(len(data_dtm.columns.values)):
    if not data_dtm.columns.values[i].isalpha() or len(data_dtm.columns.values[i]) <= 2:
        headers_with_numbers.append(data_dtm.columns.values[i])
data_dtm.drop(headers_with_numbers, axis = 1, inplace = True)

# Store the data in a csv
with open(r'D:\Repository\AI_BitBucket\ai\Project4\project4_dtm.csv', 'a') as f:
    data_dtm.to_csv(f)


# KMeans Clustering
for k in range(2, file_count):
	km = KMeans(n_clusters=k, init='k-means++')
	km.fit(data_dtm)
	sc = sklearn.metrics.calinski_harabaz_score(data_dtm, km.labels_)
	print("Checking K = " + str(k), "CHS: ", sc)

#Clusterning Analysis
def clustering_analysis(fit):
    indices = {i: np.where(fit.labels_ ==i)[0] for i in range(fit.n_clusters)}
    labels = [ data_dtm.index[i] for i, p in enumerate(data_dtm.index)]
    for index in range(fit.n_clusters):
        values = indices[int(index)]
        label  = [labels[val] for val in values]
        print(50*"-")
        print("Cluster %s Information" % index)
        for val in set(label):
            print("Value: ", str(val), "Count: ", str(label.count(val)))

# The selected k is the k with the greatest score
# K will represent the number of clusters

n_clust = 57
kmeans = KMeans(n_clusters=n_clust)
fit = kmeans.fit(data_dtm)
clustering_analysis(fit)

# Perform a hierarchical agglomeration
print(50*"-")
hac = AgglomerativeClustering(n_clusters=n_clust)
def plot_dend(hac, lim):
    """
    :param fit:
    :param lim: the limit value that will be used to cut the dandrogram
    :return:
    """
    fit = hac.fit(data_dtm[:lim])

    plot_dendrogram(fit)

    plt.show()
# Display Dandrogram of full data set
fit = plot_dend(hac, None)

# Create a dictionary that will hold 50 most common words used by each president.
# Use these words to words to identify which presidents have the most similar dspeeches
# Transpose the data_dtm to create a term document matrix then add the speeches to the dict
tdm_data = data_dtm.transpose()

top_word_dict = {}
for word in tdm_data.columns:
    top = tdm_data[word].sort_values(ascending=False).head(50)
    top_word_dict[word]= list(zip(top.index, top.values))

# print the 15 most said words by each president
most_common_words =[]
common_words_df   = pd.DataFrame()
for president, top_word in top_word_dict.items():
    prez_words = ', '.join([str(word) for word, count in top_word[0:30]])
    most_common_words.append([president, prez_words])
    common_words_df = pd.DataFrame(most_common_words, columns=['filename', 'speeches'])


data_dtm2 =  construct_a_dtm(common_words_df)
# Store the data in a csv
with open(r'D:\Repository\AI_BitBucket\ai\Project4\project4_dtm2.csv', 'a') as f:
    data_dtm2.to_csv(f)
