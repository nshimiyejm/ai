from io import StringIO
import os, os.path
import re
import string
import pandas as pd
import nltk
from nltk.tokenize import RegexpTokenizer
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

url = r'D:\Repository\AI_BitBucket\ai\Project4\speeches'
file_count = len([name for name in os.listdir(url) if os.path.isfile(os.path.join(url, name))])
df = pd.DataFrame()
president_speeches = {}


def create_corpus():
	for i in range(0, file_count):
		file_dir = os.path.join(url, os.listdir(url)[i])
		file_name = os.listdir(url)[i]
		with open(file_dir) as f:
			text = open(file_dir).read()
			president_speeches[file_name] = text
	return president_speeches

prez_and_speech = create_corpus()

# Change the data to key: president, value: speech_text format
def combined_text(list_of_text_speech):
    ''' combine a list into a large blob of text. '''
    combined_speech = ' '.join(list_of_text_speech)
    return combined_speech

speech_combined = {key: [combined_text(value)] for (key, value) in prez_and_speech.items()}

pd.set_option('max_colwidth', 150)

data_df = pd.DataFrame.from_dict(speech_combined).transpose()
data_df.columns = ['speeches']
data_df = data_df.sort_index()

# def process_files(text):
# 	'''
# 	- Change all text items to lowercase
# 	- Remove punctuation
# 	- Remove the text from a list format
# 	- Remove words containing numbers
# 	- Remove new line space character
# 	'''
# 	text = text.lower()
#
# 	# Remove new line character
# 	# text = re.sub('\n', '', text)
#
# 	# Remove square brackets and any special punctuation in the brackets
# 	# text = re.sub('\[.*?\]', '', text)
#
# 	# Remove all known puctuation marks from the text
# 	text = re.sub('[%s]' % re.escape(string.punctuation), '', text)
#
# 	# Remove words that contain numbers like 14th and years from the speeh
# 	text = re.sub('\w*\d\w*', '', text)
#
# 	return text

# cleaned_text = lambda x : process_files(x)
# clean_text   = pd.DataFrame(data_df.speeches.apply(cleaned_text))


# yourResult = [line.split(',') for line in clean_text]
# count_vect = CountVectorizer(input="file")
# docs_new = [(x) for x in yourResult ]
# X_train_counts = count_vect.fit_transform(docs_new)
#
# print(X_train_counts)

# data_dtm = pd.DataFrame(X_train_counts.toarray(), columns=count_vect.get_feature_names())
# data_dtm.index = clean_text.index
#
# print(data_dtm)





# cv = CountVectorizer(
# 					 analyzer='word',
# 					 tokenizer=None,
# 	                 preprocessor= None,
# 					 stop_words=None,
# 	                 max_features= 5000,
#                      # token_pattern= r"(?u)\b\w+\b"
#                     )
# # cv = CountVectorizer(stop_words='english')
#
# data_cv = cv.fit_transform(clean_text.speeches)
# data_dtm = pd.DataFrame(data_cv.toarray(), columns=cv.get_feature_names())
# data_dtm.index = clean_text.index
#
# print(data_dtm)



# Process each speech individually then add it to the the matrix
# for i in range(len(df.Speech)):

# tokenizer = RegexpTokenizer(r'\w+')
# tokens = tokenizer.tokenize(df.Speech[0])
# tokens = [t.lower() for t in tokens]
#
# # Remove stop words
# refined_tokens = tokens[:]
# sr = set(stopwords.words('english'))
# for token in tokens:
# 	if token in sr:
# 		refined_tokens.remove(token)
#
# lemmatizer = nltk.WordNetLemmatizer()
# lemmatized_tokens = [lemmatizer.lemmatize(w) for w in refined_tokens]
#
# freq = nltk.FreqDist(lemmatized_tokens)
#
# print('Frequency Analysis .....')
# print(75 * '-' + '\n')
# print('{}       {}'.format('Word', 'Count'))
# print('{}     {}'.format('------', '------'))
# for key, val in freq.items():
# 	print('{} : {}'.format(str(key), str(val)))
# print("Length of Unique Items: {}".format(len(freq.items())))
# freq.plot(20, cumulative=False)
#
#
#
# vec = CountVectorizer()
# X = vec.fit_transform(freq)
# print(X)
# dtm = pd.DataFrame(X.toarray(), columns=vec.get_feature_names())
# print(dtm)