# ==============================================================================
# Lab 4 Goal: CSP with Eight + Queens. Use a chess board to determine the max
#             of queens that can be used while keeping each queen safe using
#             backtracking DFS algorithm
# ==============================================================================
#   author: Brain Bennett
#   Modified by: Jean-Marie Nshimiyimana
#   last updated:  2/23/2019
# ===============================================================================
import os
import random
import sys
import time

queens = {}  # queens[i] contains the column of each queen
size = int(sys.argv[1]) # Get the board size from the command line

# Create the board as a two-dimensional list
board = [[0 for c in range(size)] for r in range(size)]

def display(pBoard):
	'''
		Generic display method that a board (an nXn list) passed via pBoard
	'''
	for r in range(size):
		line = " "
		for c in range(size):
			st = ""
			if pBoard[r][c] == 0:
				st = "Q"
			else:
				st = "."
			line += st + " "
		print(line)


def addToThreats(row, col, change):
	'''
		Propagates constraints to the:
			col - Current column passed. past the row
			diagonals - the Diagonals of row,col
		Change of +1 adds a threat (increases constraint) at each location
		Change of -1 removes a threat (decreases constraint) at each location
	'''
	# Change the column past the current row
	for j in range(0, row):
		board[j][col] += change
	# Change the diagonal beyond the current row
	for j in range(row + 1, size):
		board[j][col] += change
		if (col + (j - row) < size):
			board[j][col + (j - row)] += change
		if (col - (j - row) >= 0):
			board[j][col - (j - row)] += change


def backtracking_search(depth = 0):
	# max_depth is the total number of queens
	# that can be added to the board.When we start,
	# The board has no queen, then we add one queen
	# and propagate the threat to the adjacent v, h and d rows
	max_depth = size
	if depth == max_depth:  # Base Step
		display(board)
		return True  # Success
	else:
		# col will store the column position
		for col, value in enumerate(board[depth]):
			# The depth of is the row which starts from 0
			if board[depth][col] == 0:
				board[depth][col] = value
				# Propagate the constraints to adjacent squares
				addToThreats(depth, col, 1)
				status = backtracking_search(depth + 1) # Recursion Step
				if status:
					return True # Success
				# If you were not successful, back out of the propagation you just tried:
				# un_propagate the constraints and reevaluate where to place the queens
				addToThreats(depth, col, -1)
	return False # Failure

"""
********************************
   MAIN ALGORITHM
********************************
"""
if (not backtracking_search()):
	print("NO SOLUTION!")
