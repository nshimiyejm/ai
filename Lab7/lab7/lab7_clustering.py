#%%
import numpy as np
import matplotlib.pyplot as plt
from sklearn import metrics
from sklearn.cluster import KMeans, AgglomerativeClustering
# from lab7_dendrogram import plot_dendrogram
from sklearn.datasets import load_digits
from random import randint

#%%
# Part 1 K-Means Clustering 

digits = load_digits()
data = digits.data 
#%%
print(data.shape)
#%%
def plot_pixelgray(digits, value):
    print(50*"-")
    print(data[value])
    print(50*"-")
    print(digits.images[value])
    print(50*"-")
    print(digits.target[value])

    plt.gray()
    plt.figure(1, figsize=(3, 3))
    plt.matshow(digits.images[value])
    plt.show()
print(50*"-")
#%%
# Display an image representation of 0 
plot_pixelgray(digits, 0)

# Display an image representation of a random value 
rand_value = randint(0, 10)
plot_pixelgray(digits, rand_value)

#%%
# K-Means Clustering
print(50*"-") 
for k in range(2, 11): 
    km = KMeans(n_clusters = k , init = 'k-means++')
    km.fit(data)
    sc = metrics.calinski_harabaz_score(data, km.labels_)
    print("Checking K=" + str(k), "CHS: ", sc)

#%%
#Clusterning Analysis 
def clustering_analysis(fit):
    indices = {i: np.where(fit.labels_ ==i)[0] for i in range(fit.n_clusters)}
    labels = digits.target

    for index in range(fit.n_clusters):
        values = indices[int(index)]
        label  = [labels[val] for val in values]
        print(50*"-")
        print("Cluster %s Information" % index)
        for val in set(label):
            print("Value: ", str(val), "Count: ", str(label.count(val)))
#%%
def create_elbow(sorted_copy):
	plt.figure(figsize=(10, 8))
	wcss = []
	for i in range(1, 10):
		kmeans = KMeans(n_clusters=i, init='k-means++', random_state=100)
		kmeans.fit(sorted_copy)
		wcss.append(kmeans.inertia_)
	plt.plot(range(1, 10), wcss)
	plt.title('The Elbow Method')
	plt.xlabel('Number of clusters')
	plt.ylabel('WCSS')
	plt.show()
#%%
create_elbow(data)

#%%
n_clust = 2
kmeans = KMeans(init='k-means++', n_clusters = n_clust)
fit = kmeans.fit(data)

clustering_analysis(fit)

#%%
# Part 2 Hierachical Agglomerative Clustering 
# from lab7_dendrogram import plot_dendrogram
# print(50*"-")
# def plot_dend(fit):
#
# hac = AgglomerativeClustering(n_clusters=10)
# fit = hac.fit(data)
# plot_dendrogram(fit)
#
# plt.show()
#
# # Clustering Analysis
# clustering_analysis(fit)
#
#
# fit = hac.fit(data[:50])
# plot_dendrogram(fit)
# plt.show()