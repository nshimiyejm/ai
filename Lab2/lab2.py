# ==============================================================================
# Lab 2 Goal: Traverse a directory structure using Depth-First and Breadth-First
#        Search Algorithms
# ==============================================================================
#   author: Brain Bennett
#   Modified by: Jean-Marie Nshimiyimana
#   last updated:  2/2/2019
# ===============================================================================

import os
import time
import numpy
from queue import PriorityQueue
from collections import deque


#---------------------------------------
# The Path to Search
#  You may need to alter this based on
#   your file location and OS.
path = "./treepath"

#---------------------------------------
# The Goal Filename
#  This is the name of the files you are
#   attempting to find.
goal = "YAjrbqc.bin"
# goal = "xhtj8.bin"
# goal = "XUB.bin"

#===============================================================================
# Method: method_timing
#  Purpose: A timing function that wraps the called method with timing code.
#     Uses: time.time(), used to determine the time before an after a call to
#            func, and then returns the difference.
def method_timing(func):
    def wrapper(*arg):
        t1 = time.time()
        res = func(*arg)
        t2 = time.time()
        #print ('%s took %0.3f ms' % (func, (t2-t1)*1000.0))
        return [res,(t2-t1)*1000.0]
    return wrapper

#===============================================================================
# Method: expand
#  Purpose: Returns the child nodes of the current node in a list
#     Uses: os.listdir, which returns a Python list of children--directories
#           as well as files.
def expand(path):
        return(os.listdir(path))


#===============================================================================
# Method: breadthFirst
#  Purpose: Conducts a Breadth-First search of the file structure
#  Returns: The location of the file if it was found, an empty string otherwise.
#     Uses: Wrapped by method_timing method
@method_timing
def breadthFirst():
    # TODO: Code breadth-first search
    main_list   = deque([path])  # frontie 
    final_list = []             # visited 

    # Check if the frontier is not empty 
    while len(main_list) > 0:
        # remove the subtree root from the frontier 
        root = main_list.pop()   

        subtree_root_node = os.path.basename(root)
        full_path = os.path.abspath(root)
        
        # return the node if it is the goal of the search 
        if subtree_root_node == goal:
            return subtree_root_node

        # loop through the successor 
        else: 
            successors  = []             # Will hold the successor nodes of the root 
            counter = 0
            if os.path.isdir(full_path):
                successors = expand(full_path)
                
            for successor in successors:
                # create a full path from the root node to its successors using join 
                
                sucessor_node = os.path.join(root,successor)

                # ignore successor if it's already in the closed list 
                if sucessor_node in final_list:
                    continue
               
                if sucessor_node not in main_list:
                    main_list.appendleft(sucessor_node)


           
            # Add subtree root to the closed list 
            final_list.append(root)


#===============================================================================
# Method: depthFirst
#  Purpose: Conducts a Depth-First search of the file structure
#  Returns: The location of the file if it was found, an empty string otherwise.
#     Uses: Wrapped by method_timing method
@method_timing
def depthFirst():
    # TODO: Code depth-first search
    # TODO: Code breadth-first search
    main_list   = []   # frontie 
    final_list = []    # visited

    main_list.append(path)

    # Check if the frontier is not empty 
    while len(main_list) > 0:
        # remove the subtree root from the frontier 
        root = main_list.pop()   

        subtree_root_node = os.path.basename(root)
        full_path = os.path.abspath(root)
        
        # return the node if it is the goal of the search 
        if subtree_root_node == goal:
            return subtree_root_node

        # loop through the successor 
        else: 
            successors  = []             # Will hold the successor nodes of the root 
            if os.path.isdir(full_path):
                successors = expand(full_path)

            for successor in successors:
                # create a full path from the root node to its successors using join 
                sucessor_node = os.path.join(root,successor)
               
                # ignore successor if it's already in the closed list 
                if sucessor_node in final_list:
                    continue
               
                if sucessor_node not in main_list:
                    main_list.append(sucessor_node)
           
            # Add subtree root to the closed list 
            final_list.append(root)



#=====================
# TODO: Main Algorithm
#
#  Completing the code above will allow this code to run. Comment or uncomment
#   as necessary, but the final submission should be appear as the original.

bfs = numpy.empty((10))
for x in range(0,10):
    filelocation = breadthFirst()
    if filelocation[0] != "":
        print ("BREADTH-FIRST: Found %s in %0.3f ms" % (goal,filelocation[1]))
        bfs[x] = filelocation[1]

dfs = numpy.empty((10))
for x in range(0,10):
    filelocation = depthFirst()
    if filelocation[0] != "":
        print ("  DEPTH-FIRST: Found %s in %0.3f ms" % (goal,filelocation[1]))
        dfs[x] = filelocation[1]

print("\n FULL PATH: %s" % filelocation[0])

print ("\nBREADTH-FIRST SEARCH AVERAGE TIME: %0.3f ms" % bfs.mean())
print ("  DEPTH-FIRST SEARCH AVERAGE TIME: %0.3f ms" % dfs.mean())
