from bs4 import BeautifulSoup
import urllib.request
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize, RegexpTokenizer
from nltk.corpus import stopwords


def clean_stopwords(new_tokens):
	global clean_new_tokens
	clean_new_tokens = new_tokens[:]
	sr = stopwords.words('english')
	for token in new_tokens:
		if token in stopwords.words('english'):
			clean_new_tokens.remove(token)


def frequency_analysis(lemmatized_tokens):
	freq = nltk.FreqDist(lemmatized_tokens) # lowercase, non-punctuated tokens
	for key, val in freq.items():
		print(str(key) + ':' + str(val))
	print("Length of Unique Items: ", len(freq.items()))
	freq.plot(20, cumulative=False)



# Part 1 - Reading a Corpus from the web

print("Reading text and Tokenizing...")
response = urllib.request.urlopen('https://www.gutenberg.org/files/74/74-h/74-h.htm')
html = response.read()
# print (html)

# Cleaning the HTML - removing html tags and redering the text more readable
soup = BeautifulSoup(html, "html5lib")
text = soup.get_text(strip=True)
#print(text)
#
# Tokenization
tokens = [t for t in text.split()]

# print(tokens)
print("\n-------------------------------------------------------------\n")


# Different representation styles based on the selected
# Tokenization method library

s_tokens = sent_tokenize(text)
w_tokens = word_tokenize(text)

# print(s_tokens)
print("\n-------------------------------------------------------------\n")
# print(w_tokens)

print("\n-------------------------------------------------------------\n")
# Part 2 - Removing punctuation
print("Removing Punctuation...")
tokenizer = RegexpTokenizer(r'\w+')

new_tokens = tokenizer.tokenize(text)
new_tokens = [t.lower() for t in new_tokens]


print("Remove Stop Words...")

clean_stopwords(new_tokens)


# print(new_tokens)
print("\n-------------------------------------------------------------\n")


# Part 3 - Counting words and Eliminating Stop Words
print("Frequency Analysis...")
# frequency_analysis(clean_new_tokens)



print("\n-------------------------------------------------------------\n")

# Part 4 - Stemming and Lemmatization
#Stemming - Removing of endings from words with same meaning
print("Stemming...")

from nltk.stem import PorterStemmer

stemmer = PorterStemmer()
stemmed_tokens = [stemmer.stem(token) for token in clean_new_tokens]

print("Frequency Analysis...")
# frequency_analysis(stemmed_tokens)



print("\n-------------------------------------------------------------\n")

# Lemmatization - formation of real words - improvement of stemming
print("Lemmatization...")

from nltk.stem import WordNetLemmatizer

lemmatizer = WordNetLemmatizer()
lemmatized_tokens = [lemmatizer.lemmatize(token) for token in clean_new_tokens]

print("Frequency Analysis...")

# frequency_analysis(lemmatized_tokens)

print("\n-------------------------------------------------------------\n")
print("POS Analysis...")
import operator 
import matplotlib.pyplot as plt 

pos = nltk.pos_tag(lemmatized_tokens)
pos_counts = {}
for key, value in pos:
	print(str(key) + ':' + str(value))
	if value not in pos_counts.keys():
			pos_counts[value] = 1
	else: 
		pos_counts[value] += 1
    			
print(pos_counts)
plt.bar(range(len(pos_counts)), list(pos_counts.values()), align='center')
plt.xticks(range(len(pos_counts)), list(pos_counts.keys()))
plt.show()
print("\n-------------------------------------------------------------\n")

print("Tri-Grams...")
from nltk import ngrams
trigrams = ngrams(text.split(), 3)
for gram in trigrams: 
	print(gram)

print("\n-------------------------------------------------------------")

print("Document-Term Matrix...")
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
response = urllib.request.urlopen('https://www.gutenberg.org/files/32325/32325-h/32325-h.htm')
html = response.read()
soup = BeautifulSoup(html,"html5lib")
text2 = soup.get_text(strip=True)
docs = [text, text2]
vec = CountVectorizer()
X = vec.fit_transform(docs)
df = pd.DataFrame(X.toarray(), columns=vec.get_feature_names())
print("Instances of 'Huck' in both documents:")
print(df["huck"]) # Show the count for this word in both documents
print("Instances of 'Tom' in both documents:")
print(df["tom"]) # Show the count for this word in both documents
print(df) # Show the full data frame