# ==============================================================================
# Lab 8 Goal: Learning with Naive Bayes and Decision Tree
# ==============================================================================
#   Author: Brain Bennett
#   Modified by: Jean-Marie Nshimiyimana
#   Last updated:  4/7/2019
# ===============================================================================

import matplotlib.pyplot as plt
from sklearn import datasets


#Naive Bayes Classfication
from sklearn.naive_bayes import GaussianNB

# Decision Tree Learning
from sklearn import tree, metrics
import pydotplus


# import some data to play with
iris = datasets.load_iris()
data = iris.data

print(data)

target = iris.target
# X = data[:, :2]  # we only take the first two features.

X = data[:, 2:]  #Petal
# x = data[:, :2]  sepal
y = target

x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5

plt.figure(1, figsize=(8, 6))
plt.clf()

# Plot the training points
plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Set1,
            edgecolor='k')
# plt.xlabel('Sepal length')
# plt.ylabel('Sepal width')

plt.xlabel('Petal length')
plt.ylabel('Petal width')

plt.xlim(x_min, x_max)
plt.ylim(y_min, y_max)
plt.xticks(())
plt.yticks(())
plt.show()


# using the Naive Bayes Classification from sklearn
gnb = GaussianNB()
fit = gnb.fit(iris.data, iris.target)
prediction = fit.predict(iris.data)
#
for n, p in enumerate(prediction):
	if prediction[n] != iris.target[n]:
		print(55 * '-')
		print('Error in prediction of flower {}: {}'.format(n, data[n]))
		print('-- predicted: {} actual: {}'.format(prediction[n], iris.target[n]))



print('\n')
flowers = [ [[7.2, 3.1, 4.8, 1.5]],
            [[3.6, 2.8, 1.8, 0.5]],
            [[5.5, 3.8, 2.8, 1.2]],
            [[7.8, 1.9, 5.9, 2.1]],
            [[18.2, 9.1, 15.4, 5.5]],
            [[0.5, 0.25, 0.9, 0.3]]

          ]

# Flower class prediction
for i in range(len(flowers)):
	print(fit.predict(flowers[i]))


# Part 3 Decision Tree Learning

for i in range(10, 150, 10):

	X_train = data[:i]               # Change the numeric value for training and testing on each line
	print('X_Train:', len(X_train))
	X_test = data[i:]
	print('X_test:', len(X_test))

	Y_train = target[:i]
	Y_test = target[i:]
	clf = tree.DecisionTreeClassifier()
	clf = clf.fit(X_train, Y_train)

	y_pred = clf.predict(X_test)
	print("Accuracy:{0:.3f}".format(metrics.accuracy_score(Y_test, y_pred)), "\n")



# Creating a decision tree
with open("lab8.dot", 'w') as f:
    tree.export_graphviz(clf,out_file=f,
                        feature_names=iris.feature_names,
                        class_names=iris.target_names,
                        filled=True, rounded=True,
                        special_characters=True)
dot_data = ""
with open("lab8.dot",'r') as f:
    dot_data = f.read().replace('\n','')

# graph = pydotplus.graph_from_dot_data(dot_data)
# graph.write_pdf("lab8_tree.pdf")


#  Prediction of other flowers
for i in range(len(flowers)):
	print(clf.predict_proba(flowers[i]))

