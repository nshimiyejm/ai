#%%
#required library which holes the iris dataset
from sklearn.datasets import load_iris

#load the iris dataset
iris = load_iris()
#our inputs will contain 2 features
X = iris.data[:, [0, 2]]
#the labels are the following
y = iris.target

#%%

import numpy as np
import matplotlib.pyplot as plt


def plot_scatter(X, y):
	colors = ["red", "blue", "black", "yellow", "green", "purple", "orange"]
	markers = ('s', 'x', 'o', '^', 'v')

	for i, yi in enumerate(np.unique(y)):
		Xi = X[y == yi]
		plt.scatter(Xi[:, 0], Xi[:, 1],
		            color=colors[i], marker=markers[i], label=yi)

	plt.xlabel('X label')
	plt.ylabel('Y label')
	plt.legend(loc='upper left')
	plt.show()

#%%
# Generate the Scatterplot
plot_scatter(X, y)

#%%
y = np.where(y == 0, 1, 0)
plot_scatter(X, y)

#%%
import pandas as pd
class Preceptron(object):
	# The constructor of our class.
	def __init__(self, learningRate=0.01, n_iter=50, random_state=1):
		self.learningRate = learningRate
		self.n_iter = n_iter
		self.random_state = random_state
		self.errors_ = []


	def z(self, X):
		# np.dot(X, self.w_[1:]) + self.w_[0]
		z = np.dot(X, self.weights[1:]) + self.weights[0]
		return z


	def predict(self, X):
		# Heaviside function. Returns 1 or 0
		return np.where(self.z(X) >= 0.0, 1, 0)

	def fit(self, X, y):
		# for reproducing the same results
		random_generator = np.random.RandomState(self.random_state)

		# Step 0 = Get the shape of the input vector X
		# We are adding 1 to the columns for the Bias Term
		x_rows, x_columns = X.shape
		x_columns = x_columns + 1

		# Step 1 - Initialize all weights to 0 or a small random number
		# weight[0] = the weight of the Bias Term
		self.weights = random_generator.normal(loc=0.0, scale=0.001, size=x_columns)