#%%
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from sklearn import datasets
from sklearn import svm
from sklearn.cluster import KMeans

#%%

# import some data to play with
iris = datasets.load_iris()
data = iris.data  # Contains the four features for each flower (150 records)
target = iris.target # Contains the type of flower

#%%

X = data[:, :2]  # we only take the first two features.
y_ = target

#%%

s_length = X[:,0]
s_width = X[:,1]
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.scatter(s_length,s_width,c=target)
ax1.set_xlabel("Sepal Length (cm)")
ax1.set_ylabel("Sepal Width (cm)")
plt.show()

#%%
from mpl_toolkits.mplot3d import Axes3D

figure = plt.figure()
dataplot = figure.add_subplot(111,projection='3d')

for i in range(len(data)):
    x = data[i][0]
    y = data[i][1]
    z = target[i]
    color = ''
    if (target[i] == 0):
       color = 'r'
    elif (target[i] == 1):
       color = 'b'
    else:
       color = 'g'
    dataplot.scatter(x, y, z, marker='s', c=color)
plt.title('Iris Sepal Data in Three Dimensions ')
plt.show()

#%%
def instanciate_kmeans(data, i):
	kmeans = KMeans(n_clusters = i, init = 'k-means++', max_iter = 300, n_init = 10, random_state = 0)
	kmeans.fit(data)
	return kmeans

#%%
# Perform the clustering using n_clusters = 4
kmeans = instanciate_kmeans(X, 3)
y_kmeans = kmeans.predict(X)
labels = kmeans.labels_

#%%
# Step-3 - Training the SVM
clf = svm.SVC(kernel='rbf')
fit = clf.fit(X, labels)

#%%
sup_vec_variables = clf.support_vectors_

#%%
sup_vec_variables

#%%
x_support_v = []
y_support_v = []

for i in sup_vec_variables:
	x_support_v.append(i[0])
	y_support_v.append(i[1])


#%%
print(x_support_v)
print(y_support_v)

plt.figure(1, figsize=(8, 6))
plt.clf()
plt.title("Support Vectors ")
plt.scatter(x_support_v, y_support_v, cmap=plt.cm.Set1, edgecolors='k')
plt.show()


#%%
y1 = labels
h = .02
x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
np.arange(y_min, y_max, h))
Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
# Put the result into a color plot
Z = Z.reshape(xx.shape)
plt.contourf(xx, yy, Z, cmap=plt.cm.Paired, alpha=0.8)
# Plot also the training points
plt.scatter(X[:, 0], X[:, 1], c=y1, cmap=plt.cm.Paired)

plt.xlabel('X')
plt.ylabel('Y')
plt.xlim(xx.min(), xx.max())
plt.ylim(yy.min(), yy.max())
plt.xticks(())
plt.yticks(())
plt.title("RBF Separators")
plt.show()


#%%
# Step 4: Using the SVM Result to Classify Data
test_data = [
[ 7.4, 3.4],
[ 5.0, 1.5],
[ 4.75, 4.0],
[5.9, 3.5],
[3.5, 1.1]
]
labels_names = ['Setosa' , 'Versicolour', 'Virginica']
prediction_list = []
for i in test_data:
	prediction = clf.predict([i])
	prediction_list.append(prediction[0])
	print('{} - {}'.format(prediction[0], kmeans.cluster_centers_[prediction[0]]))

#%%
# Convert test_data to an np array
t_data = np.array(test_data)

# The target list list is the prediction_list
X1 = t_data
y1 = prediction_list
fit = clf.fit(X1, y1)

#%%

# Put the result into a color plot
Z = Z.reshape(xx.shape)
plt.figure(figsize=(20, 15))
plt.contourf(xx, yy, Z, cmap=plt.cm.Paired, alpha=0.8)
# Plot also the training points
plt.scatter(X1[:, 0], X1[:, 1], c=y1, cmap=plt.cm.Paired, s=1000)

plt.scatter(X[:, 0], X[:, 1], c=y_, cmap=plt.cm.Paired, s=200)

plt.xlabel('X')
plt.ylabel('Y')
plt.xlim(xx.min(), xx.max())
plt.ylim(yy.min(), yy.max())
plt.xticks(())
plt.yticks(())
plt.title("RBF Separators")
plt.show()