#%%
#--------------------------------------------------------------------------------------------------------
#  Lab10: SVM
#  Author: Jean Marie Nshimiyimana
#  Date Created: 4/15/19
#  Last Update: 4/19/19
#  Relevant Sources: https://www.kaggle.com/tonzowonzo/simple-k-means-clustering-on-the-iris-dataset
#--------------------------------------------------------------------------------------------------------

#import libraries
#%%
import numpy as np
# import sys
import matplotlib.pyplot as plt

# filename = ""
# if len(sys.argv) == 3:
# 	filename = sys.argv[1]
# else:
# 	sys.exit(-1)

data = np.loadtxt(r"D:\Repository\AI_BitBucket\ai\Lab10\lab10_linear.txt")

#%%
# Display content in data to verify that the data was correctly entered
data

#%%
# Step-1 - Load and Plot unlabeled data
x = []
y = []

#%%
# Plotting the available clusters

print(data[0])
print(data[1])

#%%
for row in data:
	x.append(int(row[0]))
	y.append(int(row[1]))

plt.figure(1, figsize=(8, 6))
plt.clf()
plt.title("Unlabeled Data")
plt.scatter(x, y, cmap=plt.cm.Set1, edgecolors='k')
plt.show()

#%%
# Step-2 - Using KMeans to label the data
# Import sklearn modules that will be used for clustering
from sklearn.cluster import KMeans

#%%
# Using the elbow method to determine how many clusters are needed
# Once the number of clusters has been determined, then perform the
# Clustering and then label the created clusters.
def create_elbow(data):
	plt.figure(figsize=(10, 8))
	wcss = []
	for i in range(1, 10):
		kmeans = instanciate_kmeans(data, i)
		wcss.append(kmeans.inertia_)
	plt.plot(range(1, 10), wcss)
	plt.title('The Elbow Method')
	plt.xlabel('Number of clusters')
	plt.ylabel('WCSS')
	plt.show()

def instanciate_kmeans(data, i):
	kmeans = KMeans(n_clusters = i, init = 'k-means++', max_iter = 300, n_init = 10, random_state = 0)
	kmeans.fit(data)
	return kmeans


#%%
# Use the Elbow method to estimate the number of clusters to use
create_elbow(data)

#%%
# Perform the clustering using n_clusters = 4
kmeans = instanciate_kmeans(data, 4)
y_kmeans = kmeans.predict(data)

#%%
labels = kmeans.labels_
#%%
labels_names = ['Setosa' , 'Versicolour', 'Virginica', 'Unknown']
colors = ['#9b59b6', '#3498db'    , '#95a5a6'  , 'orange']

#%%
# Plotting the clusters
for i in range(4):
	plt.scatter(data[y_kmeans == i, 0], data[y_kmeans == i, 1], s = 5, c = colors[i],  label = labels_names[i])
# Plotting the centroids of the clusters
plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:,1], s = 10, c = 'yellow', label = 'Centroids')

plt.legend()
plt.show()

#%%
# Step-3 - Training the Support Vector Machine
from sklearn import svm

clf = svm.SVC(kernel='linear')
fit = clf.fit(data, labels)

#%%
sup_vec_variables = clf.support_vectors_

#%%
x_support_v = []
y_support_v = []

for i in sup_vec_variables:
	x_support_v.append(i[0])
	y_support_v.append(i[1])

#%%
print(x_support_v)
print(y_support_v)

plt.figure(1, figsize=(8, 6))
plt.clf()
plt.title("Support Vectors ")
plt.scatter(x_support_v, y_support_v, cmap=plt.cm.Set1, edgecolors='k')
plt.show()

#%%
# Import data from the iris dataset then keep 2 features
from sklearn import datasets
iris = datasets.load_iris()
X = data  # Same data from the file

#%%
y = labels
h = .02
x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
np.arange(y_min, y_max, h))
Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
# Put the result into a color plot
Z = Z.reshape(xx.shape)
plt.contourf(xx, yy, Z, cmap=plt.cm.Paired, alpha=0.8)
# Plot also the training points
plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Paired)

plt.xlabel('X')
plt.ylabel('Y')
plt.xlim(xx.min(), xx.max())
plt.ylim(yy.min(), yy.max())
plt.xticks(())
plt.yticks(())
plt.title("Linear Separators")
plt.show()

#%%
# Step 4: Using the SVM Result to Classify Data
test_data = [
[12.0 ,-15.0],
[-12.0 ,-15.0],
[-10.0, 20.0],
[2.9, 5.3],
[8.7, 0.28]
]
prediction_list = []
for i in test_data:
	prediction = clf.predict([i])
	prediction_list.append(prediction[0])
	print('{} - {}'.format(prediction[0], kmeans.cluster_centers_[prediction[0]]))

#%%
# Convert test_data to an np array
t_data = np.array(test_data)

# The target list list is the prediction_list
y1 = prediction_list
clf = svm.SVC(kernel='linear')
fit = clf.fit(t_data, y1)

#%%
h = .02
x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
np.arange(y_min, y_max, h))
Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
# Put the result into a color plot
Z = Z.reshape(xx.shape)
plt.contourf(xx, yy, Z, cmap=plt.cm.Paired, alpha=0.8)
# Plot also the training points
plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Paired)
plt.scatter(t_data[:, 0], t_data[:, 1], c=y1, cmap=plt.cm.Paired, s=100)

plt.xlabel('X')
plt.ylabel('Y')
plt.xlim(xx.min(), xx.max())
plt.ylim(yy.min(), yy.max())
plt.xticks(())
plt.yticks(())
plt.title("Linear Separators")
plt.show()