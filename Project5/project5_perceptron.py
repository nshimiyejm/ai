
import numpy as np
import pandas as pd


class Perceptron:
    """ Perceptron classifier """

    def __init__(self, n_inputs, learning_rate, iterations):
        self.n_inputs = n_inputs
        self.learning_rate = learning_rate
        self.iterations = iterations
        self.w_ = np.zeros(1)
        self.errors_ = []


    def train(self, X, y):
        """
            Fits the training data to the Perceptron
        """

        #TODO: Code the Perceptron Learning Algorithm
        # (1)	Initialize weights to zero or a small random number.
        #        Don't forget w_[0] is the bias weight
        # (2)	For the number of iterations,
        #       (a)	For each item in data set X:
        #         (i)	Activate the answer
        #         (ii)	Calculate the error based on the answer and the known
        #                  y[i]
        #         (iii)	Update the bias weight and input weights if the
        #                  activated answer is erroneous
        #       (b)	Record the error in self.errors_

        #
        random_generator = np.random.RandomState(self.n_inputs)
        x_row, x_col = X.shape
        x_col = x_col + 1

        # 1. Initialize weight to 0 or smaller value
        self.w_ = random_generator.normal(loc=0.0, scale=0.001, size=len(X[0]) + 1)
        print(self.w_)

        # For the number of iterations,
        for _ in range(self.iterations):
            #Create an error variable that will be added to self.errors__
            errors = 0
            # For each item in data set X:
            for xi, y_value in zip(X, y):
                # Activate the answer
                y_answer = self.activate(xi)
                # Calculate the error based on the answer and the known
                delta = self.learning_rate * (y_value - y_answer)
                # Update the bias weight and input weights if the activated answer is erroneous
                self.w_[1:] += delta * xi
                self.w_[0]  += delta
                errors += int(delta != 0.0)
            # Record the error in self.errors_
            self.errors_.append(errors)

    def net_input(self, X):
        """ Calculate the Net Input """
        # TODO: Calculate the sum of the product of each input and each weight
        net_input_ = np.dot(X, self.w_[1:]) + self.w_[0]
        return net_input_

    def activate(self, X):
        """ STEP FUNCTION: Returns the class label after the unit step """
        # TODO: Return 1 if net_input(X) >= 0.0 or -1 otherwise
        return np.where(self.net_input(X) >= 0.0, 1, -1)
